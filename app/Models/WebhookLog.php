<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WebhookLog extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'webhook_id',
        'request',
        'response',
        'status',
        'created_at',
        'finished_at',
    ];
}
